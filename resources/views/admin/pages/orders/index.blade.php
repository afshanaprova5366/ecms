@extends('admin.master')

@section('content')


<div class="container">
    <h1>Orders</h1>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>User</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Status</th>
                <th>Total Price</th>
                
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->receiver_name }}</td>
                    <td>{{ $order->receiver_email }}</td>
                    <td>{{ $order->receiver_mobile }}</td>
                    <td>{{ $order->status }}</td>
                    <td>{{ $order->total_price }}</td>
                    {{-- <td>
                        <a href="{{ route('admin.orders.show', $order) }}" class="btn btn-primary">View</a>
                        <!-- Additional actions/buttons can be added here -->
                    </td> --}}
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $orders->links() }} <!-- Pagination links -->
</div>







@endsection
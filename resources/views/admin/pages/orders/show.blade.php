@extends('admin.master')

@section('content')
    <div class="container">
        <h1>Order Details</h1>
        <p><strong>Order ID:</strong> {{ $order->id }}</p>
        <p><strong>User:</strong> {{ $order->user->name }}</p>
        <!-- Display other order details here -->
        <!-- Example: <p><strong>Status:</strong> {{ $order->status }}</p> -->
        <!-- You can customize this view based on your requirements -->
    </div>
@endsection